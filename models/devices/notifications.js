'use strict';
var log = require('../../config/log')(module);

var fs = require('fs');
var apn = require('apn');
var gcm = require('node-gcm');

var options = {
  cert: fs.readFileSync(__dirname + '/../../cert/veemobile.pem'),
  key: fs.readFileSync(__dirname + '/../../cert/veemobileKey.pem'),
  passphrase: 'veemobile',
  production: false,
  batchFeedback: true,
  interval: 300,
};

function Notification() {
  this.apnConnection = new apn.Provider(options);
  this.gcmConnection = new gcm.Sender('AIzaSyANtMbHm9_lE3EFHXFRXGhyusbxaBudYak');

}


Notification.prototype = {
  push: function(deviceId, message, callback) {
    log.info('Sending message', deviceId, message);

    // TODO: query devices with deviceId, retrieve registrationToken, pass token
    var messageToken = 'eYk_XeJPGhA:APA91bExEQ3boR35GKHY2C0nQgXRD1bQwAEHlpXrAfSqYV2DZncGmMS68W_Yw2I1eyq-SnRe9vUXcha4X8i0py_-FIJS8Bz6fYwlyP857c-tMOt86D8D1nvC8TuZVkPUbHPKBbqXN1c-';

    this.pushAndroid(messageToken, message, function(err, response) {
      //callback(err, response);
    });

    messageToken = '028bd3604af87d81a40fb9eab583b2a7cdc4c06faae90f5f0140b02b6e8212c8';
    this.pushIos(messageToken, message, function(err, response) {
      //callback(err, response);
    });


    callback(null, 'sent');
  },
  pushIos: function(messageToken, message, callback) {
    //var myDevice = new apn.Device(messageToken);
    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.badge = message.badge;
    note.sound = message.sound;
    note.alert = {
      title: message.title,
      body: message.body
    };
    note.payload = {messageFrom: message.payload};

    //this.apnConnection.pushNotification(note, myDevice);
    this.apnConnection.send(note, messageToken).then(function(response) {
      response.sent.forEach(function(token) {
        log.info('Note sent', token);
      });
      response.failed.forEach(function(failure) {
        if (failure.error) {
          log.error(failure.error);
        } else {
          log.error('Error pushing notification', token, failure.status, failure.response);
        }
      });
    });
    callback(null, {});

  },
  pushAndroid: function(messageToken, message, callback) {
    var registrationTokens = [];
    //registrationTokens.push('eYk_XeJPGhA:APA91bExEQ3boR35GKHY2C0nQgXRD1bQwAEHlpXrAfSqYV2DZncGmMS68W_Yw2I1eyq-SnRe9vUXcha4X8i0py_-FIJS8Bz6fYwlyP857c-tMOt86D8D1nvC8TuZVkPUbHPKBbqXN1c-');
    registrationTokens.push(messageToken);

    var notification = new gcm.Message();
    if (message.payload !== {}) {
      notification.addData(message.payload);
    }


    notification.addNotification({
      title: message.title,
      body: message.body,
      icon: 'ic_launcher',
    });

    this.gcmConnection.send(notification, registrationTokens, function(err, response) {
      if (err) {
        log.error(err);
        callback(err);
      } else {
        log.info(response);
        callback(null, response);
      }
    });
  }


};


module.exports = Notification;
