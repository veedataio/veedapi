'use strict';

var MyStream = require('json2csv-stream');
//var parser = new MyStream();
var through2 = require('through2');

var fs = require('fs');

//var objectStream = {};
//var jsonStream = {};

/**
function init() {
  console.log('initializing streams');
  objectStream = through2.obj(function(chunk, encoding, callback) {
    this.push(chunk);
    callback();
  });
  jsonStream = through2.obj(function(chunk, encoding, callback) {
    this.push(JSON.stringify(chunk, null, 4) + ',');
    callback();
  });

  jsonStream.on('end', function() {

    console.log('reached the end.');
  });

}
*/

function CSV() {
  this.jsonStream = undefined;
  this.objectStream = undefined;
  this.parser = new MyStream();

  this.parser.on('header', function(data) {
    //console.log(data);
  });

  this.parser.on('line', function(data) {
    //console.log(data);
  });

  this.parser.on('end', function(data) {

    console.log('reached end: ' + data);


  });
}

CSV.prototype = {
  init: function() {
    console.log('initializing streams');
    this.objectStream = through2.obj(function(chunk, encoding, callback) {
      this.push(chunk);
      callback();
    });
    this.jsonStream = through2.obj(function(chunk, encoding, callback) {
      this.push(JSON.stringify(chunk, null, 4) + ',');
      callback();
    });

    this.jsonStream.on('end', function() {

      console.log('reached the end.');
    });


  },

  export: function(callback) {

    console.log('starting to export');

    //var writer = fs.createWriteStream('./out.csv');
    //objectStream.pipe(jsonStream).pipe(parser).pipe(writer);
    this.objectStream.end();
    callback(this.objectStream.pipe(this.jsonStream).pipe(this.parser));



  },

  add: function(data, islast) {

    this.objectStream.write(data);

  },
  destroy: function() {

    delete this.jsonStream;


    delete this.objectStream;
  }
};

module.exports = CSV;