'use strict';
const codename = require('codename')();
const log = require('../config/log')(module);
const moment = require('moment');
const util = require('util');
const uuid = require('node-uuid');

const DeviceRegistry = require('../models/deviceRegistry');
const DocumentDbObject = require('../db/docDbObject');
const documentDb = new DocumentDbObject();
const dbStructure = require('../db/docDb');
documentDb.init(dbStructure.veedb.databases.service.collections.devices, null);

var Device = function(data) {

  // initialize default values
  if (data.id !== undefined) {
    this.data.id = data.id;
  }

  if (data.alias !== undefined) {
    this.data.alias = data.alias;
  }

  if (data.syncSessions !== undefined) {
    this.data.syncSessions = data.syncSessions;
  }

  if (data.userSessions !== undefined) {
    this.data.userSessions = data.userSessions;
  }

};

Device.prototype = {
  data: {
    id: null,
    alias: null,
    syncSessions: [],
    userSessions: []
  },

  createSyncSession: function(startTime, callback) {
    var _this = this;
    var newSession = {
      sessionId: uuid.v4(),
      startTime: startTime,
      endTime: ''
    };

    var numberOfSessions = _this.data.syncSessions.length;

    // if last session doesn't have an end time then a session is opened already
    if (numberOfSessions > 0 && _this.data.syncSessions[numberOfSessions - 1].endTime === '') {
      callback(new Error('Can\'t start a sync session, "' + _this.data.id + '" has an opened session already.'));

    } else {
      /*
       * Copy the data temporarily. If the update succeeds, then copy the replaced (updated) document into _this.data,
       * else _this.data remains intact.
       */
      var tempData = JSON.parse(JSON.stringify(_this.data));
      tempData.syncSessions.push(newSession);

      documentDb.updateItem(tempData, function(err, replaced) {
        if (err) {
          log.error('Saving device data with a new sync session failed. Error: ' + JSON.stringify(err));
          callback(err);

        } else {
          _this.data = replaced;

          log.info('Saving device data with new sync session succeeded.');
          log.debug(_this.data);

          callback(null, newSession);
        }
      });
    }
  },

  endSyncSession: function(endTime, callback) {
    var _this = this;
    var numberOfSessions = _this.data.syncSessions.length;

    // existing sessions? last one has an end time? then last session has ended already
    if (numberOfSessions >= 0 && _this.data.syncSessions[numberOfSessions - 1].endTime !== '') {
      callback(new Error('Can\'t end a sync session, "' + this.data.id + '" has not an opened session.'));

    } else {
      /*
       * Copy the data temporarily. If the update succeeds, then copy the replaced (updated) document into _this.data,
       * else _this.data remains intact.
       */
      var tempData = JSON.parse(JSON.stringify(_this.data));
      tempData.syncSessions[numberOfSessions - 1].endTime = endTime;

      documentDb.updateItem(tempData, function(err, replaced) {
        if (err) {
          log.error('Saving device data with a new end time for sync session failed. Error: ' + JSON.stringify(err));
          callback(err);

        } else {
          _this.data = replaced;

          log.info('Saving device data with a new end time for sync session succeeded.');
          log.debug(_this.data);

          callback(null, _this.data.syncSessions[numberOfSessions - 1]);
        }
      });
    }

  },

  createUserSession: function(userId, startTime, callback) {
    var _this = this;

    var lastSession = _findLastUserSession(_this.data);

    // if last session belongs to the give user and it is still running, then send back the current session
    if (!_isEmptyObject(lastSession) && lastSession.userId === userId && lastSession.endTime === '') {
      callback(null, lastSession, false);

    } else {
      var newSession = {
        'sessionId': uuid.v4(),
        'userId': userId,
        'startTime': startTime,
        'endTime': ''
      };

      /*
       * Copy the data temporarily. If the update succeeds, then copy the replaced (updated) document into _this.data,
       * else _this.data remains intact.
       */
      var tempData = JSON.parse(JSON.stringify(_this.data));

      /*
       * if the last session is still running, set the end time.
       * then remove the outdated lastSession from the temp data and push the updated lastSession.
       */
      if (!_isEmptyObject(lastSession) && lastSession.endTime === '') {
        lastSession.endTime = startTime;
        tempData.userSessions.pop();
        tempData.userSessions.push(lastSession);
      }

      tempData.userSessions.push(newSession);

      documentDb.updateItem(tempData, function(err, replaced) {
        if (err) {
          log.error('Saving device data with a new user session failed. Error: ' + JSON.stringify(err));
          callback(err);

        } else {
          _this.data = replaced;

          log.info('Saving device data with new user session succeeded.');
          log.debug(_this.data);

          callback(null, newSession, true);
        }
      });
    }
  },

  endUserSession: function(userId, endTime, callback) {
    var _this = this;

    var lastSession = _findLastUserSession(_this.data);

    if (_isEmptyObject(lastSession)) {
      callback(new Error('"' + _this.data.id + '" doesn\'t have user sessions.'));

    } else if (lastSession.userId !== userId) {
      callback(new Error('"' + userId + '" doesn\'t have a session with "' + _this.data.id + '".'));

    } else if (lastSession.endTime !== '') {
      callback(new Error('"' + userId + '" has previously ended its session with "' + _this.data.id + '".'));

    } else {
      /*
       * Copy the data temporarily. If the update succeeds, then copy the replaced (updated) document into _this.data,
       * else _this.data remains intact.
       */
      var tempData = JSON.parse(JSON.stringify(_this.data));

      /*
       * last session is still running, set the end time, remove the outdated lastSession from the temp data and
       * then push the updated lastSession
       */
      lastSession.endTime = endTime;
      tempData.userSessions.pop();
      tempData.userSessions.push(lastSession);

      documentDb.updateItem(tempData, function(err, replaced) {
        if (err) {
          log.error('Saving device data with an ended user session failed. Error: ' + JSON.stringify(err));
          callback(err);

        } else {
          _this.data = replaced;

          log.info('Saving device data with an ended user session succeeded.');
          log.debug(_this.data);

          callback(null, lastSession, true);
        }
      });
    }
  },

  getId: function() {
    return this.data.id;
  },

  getIotHubInfo: function(callback) {
    DeviceRegistry.getConnectionString(this.getId(), function(err, connString) {
      var iotHubInfo = {'connString': connString};
      callback(err, iotHubInfo);
    });
  },

  getLastUserSession: function() {
    return _findLastUserSession(this.data);
  }
};

Device.delete = function(deviceId, callback) {
  log.info('Device.delete: Deleting device (id=%s)', deviceId);
  DeviceRegistry.delete(deviceId, function(err) {
    if (err) {
      callback(err);
    } else {
      documentDb.deleteItem(deviceId, function(err, result) {
        if (err) {
          callback(err);
        } else {
          log.info('Device.delete device successfully deleted: ', deviceId);
          callback(null);
        }
      });
    }
  });
};

Device.update = function(newDevice, callback) {
  log.info('Device.update: Updating device(id=%s)', newDevice.id);
  documentDb.updateItem(newDevice, function(err, result) {
    callback(err, result);
  });
};

Device.create = function(alias, userId, callback) {
  log.info('Device.create: Creating a new device (alias=%s, user=%s)', alias, userId);
  DeviceRegistry.create(function(err, deviceId) {
    if (err) {
      callback(err);
    } else {
      var deviceData = {
        id: deviceId
      };

      if (alias != undefined && alias !== '' && alias !== 'undefined') {
        deviceData.alias = alias;
      } else {
        // auto generates an alias, see: https://github.com/rjz/codename
        deviceData.alias = codename.generate(['alliterative', 'unique', 'random'], ['crayons'])[0];
      }

      if (userId != undefined && userId !== '' && userId !== 'undefined') {
        deviceData.userSessions = [
          {
            sessionId: uuid.v4(),
            userId: userId,
            startTime: moment.utc(),
            endTime: ''
          }
        ];
      }

      log.debug('Device.create deviceData =', deviceData);
      documentDb.addItem(deviceData, function(err, result) {
        if (err) {
          callback(err);
        } else {
          log.info('Device.create new device successfully created: ', deviceData);
          callback(err, _buildDeviceSummary(result));
        }
      });
    }
  });
};

Device.findById = function(id, callback) {
  documentDb.getItem(id, function(err, result) {
    if (err) {
      return callback(err);

    } else if (result == null || result.length === 0) {
      return callback(new Error('Device "' + id + '" not found.'));

    } else {
      log.debug('Device.findById = ' + JSON.stringify(result));
      callback(null, new Device(result));
    }
  });
};

Device.getAllDevices = function(callback) {
  var colDevices = dbStructure.veedb.databases.service.collections.devices;
  documentDb.find('SELECT * FROM ' + colDevices, function(err, results) {
    if (err) {
      var error = {
        message: 'Query to collection ' + colDevices + ' failed.',
        cause: err
      };
      log.error(error);
      callback(error);

    } else {
      var deviceSummaryArray = [];
      for (var i = 0; i < results.length; i++) {
        deviceSummaryArray.push(_buildDeviceSummary(results[i]));
      }

      callback(null, deviceSummaryArray);
    }

  });
};

function _buildDeviceSummary(deviceData) {
  // if the last user session has an end time, then no one is using the device
  // otherwise use the user id from the last session
  var lastUserSession = _findLastUserSession(deviceData);
  var currentUser;
  if (lastUserSession.endTime) {
    currentUser = '';

  } else {
    currentUser = lastUserSession.userId;
  }

  var lastSync = _findLastSyncSession(deviceData);

  return {
    id: deviceData.id,
    alias: deviceData.alias,
    lastSync: lastSync.startTime,
    currentUser: currentUser
  };
}

function _findLastUserSession(device) {
  if (!device.hasOwnProperty('userSessions') || device.userSessions.length <= 0) {
    return {};

  } else {
    var numOfUserSessions = device.userSessions.length;
    return device.userSessions[numOfUserSessions - 1];
  }
}

function _findLastSyncSession(device) {
  if (!device.hasOwnProperty('syncSessions') || device.syncSessions.length <= 0) {
    return {};

  } else {
    var numOfSessions = device.syncSessions.length;
    return device.syncSessions[numOfSessions - 1];
  }
}

function _isEmptyObject(obj) {
  return !Object.keys(obj).length;
}

module.exports = Device;
