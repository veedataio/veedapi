'use strict';
var log = require('../config/log')(module);
var config = require('../config/config');
var dbUrl = config.dbUrl;
var dbMasterKey = config.dbMasterKey;

var async = require('async');
var util = require('util');
var fs = require('fs');
var UUID = require('uuid-1345');
var path = require('path');
var moment = require('moment');
var database = 'messages';
var collection = 'data';
var DocumentClient = require('documentdb').DocumentClient;
var WrappedClient = require('documentdb-utils').WrappedClient;


var _client = new DocumentClient(dbUrl, {
  masterKey: dbMasterKey,
});
var client = new WrappedClient(_client);


var OLAPCube, cube;

OLAPCube = require('lumenize').OLAPCube;


var _getData = function(deviceId, dataType, dataSubtype, startTime, endTime, callback) {

  var query = '' +
    'SELECT r.deviceId, r.dataType, r.dataSubtype, r.data ' +
    'FROM root r ' +
    'WHERE r.deviceId = @deviceId ' +
    'AND r.dataType = @dataType ' +
    'AND r.dataSubtype = @dataSubtype ' +
    'AND (r.data.timestamp BETWEEN @startTime AND @endTime) ' +
    'ORDER BY r._ts asc';

  var querySpec = {
    query: query,
    parameters: [
      {
        name: '@deviceId',
        value: deviceId,
      },
      {
        name: '@dataType',
        value: dataType,
      },
      {
        name: '@dataSubtype',
        value: dataSubtype,
      },
      {
        name: '@startTime',
        value: startTime,
      },
      {
        name: '@endTime',
        value: endTime,
      }
    ],
  };

  log.debug('Query: ' + util.inspect(querySpec));

  var queryiterator = client.queryDocuments('dbs/' + database + '/colls/' + collection, querySpec);
  var allResults = [];
  execute(queryiterator, querySpec, function(results, isdone) {

    if (isdone) {
      callback(allResults);
      return;
    }
    results.forEach(function(result) {
      allResults.push(result);
    });
  });

};

var _getDataAsFile = function(deviceId, dataType, dataSubtype, startTime, endTime, fileType, callback) {

  var query = '' +
    'SELECT r.deviceId, r.dataType, r.dataSubtype, r.data ' +
    'FROM root r ' +
    'WHERE r.deviceId = @deviceId ' +
    'AND r.dataType = @dataType ' +
    'AND r.dataSubtype = @dataSubtype ' +
    'AND (r.data.timestamp BETWEEN @startTime AND @endTime) ' +
    'ORDER BY r._ts asc';

  var querySpec = {
    query: query,
    parameters: [
      {
        name: '@deviceId',
        value: deviceId,
      },
      {
        name: '@dataType',
        value: dataType,
      },
      {
        name: '@dataSubtype',
        value: dataSubtype,
      },
      {
        name: '@startTime',
        value: startTime,
      },
      {
        name: '@endTime',
        value: endTime,
      }
    ],
  };

  log.debug('Query: ' + util.inspect(querySpec));

  var queryiterator = client.queryDocuments('dbs/' + database + '/colls/' + collection, querySpec);
  var allResults = [];
  var fileName = UUID.v4fast() + '.json';
  var filePath = path.join(__dirname, '..', 'public', 'tmp');

  var writeCounter = 0;
  var wstream = fs.createWriteStream(filePath + '/' + fileName);
  wstream.write('[');

  execute(queryiterator, querySpec, function(results, isdone) {

    // Write to file
    for (var result in results) {
      var writeResult = results[result];
      // remove unecessary fields
      delete writeResult.data._id;
      delete writeResult.data.sync_state;

      if (writeCounter == 0) {
        wstream.write(JSON.stringify(writeResult));
        writeCounter++;
      } else {
        wstream.write(',' + JSON.stringify(writeResult));
        writeCounter++;
      }
    }


    if (isdone) {
      wstream.write(']');
      wstream.end();

      wstream.on('finish', function() {
        log.debug('file has been written in ' + writeCounter + ' attempts.');
        var result = {
          url: 'tmp/' + fileName,
        };

        callback(null, result);
      });


      return;
    }
    results.forEach(function(result) {
      allResults.push(result);
    });
  });

};

var _getDataAggregated = function(deviceId, dataType, dataSubtype, startTime, endTime, interval, callback) {
  // dirty hack to create intervals based on min/hour/day/month/year
  var offset = 16; // 2016-10-20T17:25:23.843Z
  if (interval === 'hour') {
    offset = 13;
  } else if (interval === 'day') {
    offset = 10;
  } else if (interval === 'month') {
    offset = 7;
  }

  var filterQuery = '' +
    'SELECT r.data.heartRate, r.data.quality, SUBSTRING(r.data.timestamp, 0,' + offset + ') AS interval ' +
    'FROM root r ' +
    'WHERE r.dataSubtype = "heartRate"' +
    'AND r.deviceId = "' + deviceId + '" ' +
    'AND (r.data.timestamp BETWEEN "' + startTime + '" AND "' + endTime + '") ';

  var cubeConfig = {groupBy: 'interval', field: 'heartRate', f: 'average'};
  var memo = {cubeConfig: cubeConfig, filterQuery: filterQuery};
  var sprocLink = 'dbs/' + database + '/colls/' + collection + '/sprocs/cube';

  executeStoredProcedure(sprocLink, memo, function(err, response) {
    log.info(response);
    var returnData = {
      id: deviceId,
      dataType: dataType,
      dataSubtype: dataSubtype,
      startTime: startTime,
      endTime: endTime,
      interval: interval,
      calculated: {
        average: 0,
      },
      data: []
    };
    var results = response.savedCube.cellsAsCSVStyleArray;
    var sum = 0;
    for (var result in results) {
      // exclude headers
      if (result == 0) {
        continue;
      }
      sum += results[result][3];
      // convert timestamp and push right data
      returnData.data.push({
        timestamp: moment.utc(results[result][0]).toJSON(),
        value: results[result][3],
      })
    }
    returnData.calculated.average = sum / (results.length - 1);
    callback(null, returnData);
  });

};

function executeStoredProcedure(sprocLink, memo, callback) {
  client.executeStoredProcedure(sprocLink, memo, function(err, response, headers) {
    log.charge('q3', {charge: headers['x-ms-request-charge'], querySpec: sprocLink});
    log.error(err);

    if (err && err.code === 429 && headers['x-ms-retry-after-ms']) {

      var retryInMs = headers['x-ms-retry-after-ms'];
      log.debug('Request size too large, retry in %s', retryInMs);
      // Request size too large, retry after ms
      setTimeout(function() {
        executeStoredProcedure(sprocLink, memo, callback)
      }, retryInMs);

    } else if (err && err.code !== 429) {
      log.error(err);

    } else {
      log.debug('headers: ' + headers);
      callback(null, response);
    }

    //cube = OLAPCube.newFromSavedState(response.memo.savedCube);
    //console.log(cube.toString());

  });
}

function execute(queryiterator, querySpec, callback) {
  queryiterator.executeNext(function(err, docs, headers) {
    log.charge('q1', {charge: headers['x-ms-request-charge'], querySpec: querySpec});

    if (err && err.code === 429 && headers['x-ms-retry-after-ms']) {

      var retryInMs = headers['x-ms-retry-after-ms'];
      log.debug('Request size too large, retry in %s', retryInMs);
      // Request size too large, retry after ms
      setTimeout(function() {
        execute(queryiterator, querySpec, callback);
      }, retryInMs);

    } else if (err && err.code !== 429) {
      log.error(err);

    } else {
      log.debug('headers: ' + headers);

      if (callback) {
        callback(docs, false);
      }

      if (queryiterator.hasMoreResults()) {
        execute(queryiterator, querySpec, callback);
      } else {
        callback(null, true);
      }
    }

  });

}

module.exports = {
  getData: _getData,
  getDataAsFile: _getDataAsFile,
  getDataAggregated: _getDataAggregated,
};
