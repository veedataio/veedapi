'use strict';

var async = require('async');

var DocumentObject = require('../db/docDbObject');
var feedDoc = new DocumentObject();
var config = require('../db/docDb');
var log = require('../config/log')(module);

var feedColl = config.veedb.databases.service.collections.feed;
feedDoc.init(feedColl, null);

var _findAll = function(callback) {
  feedDoc.find('select * from ' + feedColl, function(err, results) {
    if (err) {
      log.error('DocumentDB: ', err);
      return;
    }

    callback(results);

  });
};

var _findByTags = function(tags, callback) {
  var query = 'SELECT * From ' + feedColl + ' WHERE';
  for (var tag in tags) {
    query += ' ARRAY_CONTAINS(' + feedColl + '.tags,"' + tags[tag] + '")';

    if (parseInt(tag) !== (tags.length - 1)) {
      query += ' OR';
    }
  }

  log.debug(query);
  feedDoc.find(query, function(err, results) {
    if (err) {
      log.error('DocumentDB: ', err);
      return;
    }

    callback(results);
  });
};

var _find = function(id, callback) {

  feedDoc.getItem(id, function(err, result) {
    if (err) {
      log.error('DocumentDB: ', err);
      return;
    }

    if (result.length == 0) {
      callback({
        message: 'Document with id ' + id + ' could not be found.'
      });
    } else {
      callback(result);
    }

  });
};

var _add = function(feed, callback) {
  feedDoc.addItem(feed, function(err, newDocument) {
    if (err) {
      callback({
        message: 'Document could not be added.',
      });
    } else {
      callback(newDocument);
    }

  });
};

var _update = function(feed, callback) {
  feedDoc.updateItem(feed, function(err, result) {
    callback(err, result);
  });
};

var _delete = function(id, callback) {
  feedDoc.deleteItem(id, function(err, result) {
    callback(err, result);
  });

};

module.exports = {
  findAll: _findAll,
  find: _find,
  add: _add,
  findByTags: _findByTags,
  update: _update,
  delete: _delete,
};