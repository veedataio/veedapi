'use strict';


function Healthcheck(options) {
  if (!options) {
    options = {};
  }

  this.version = options.version;
  this.alive = options.alive;
}

module.exports = Healthcheck;