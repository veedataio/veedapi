'use strict';

var DocumentObject = require('../db/docDbObject');
var document = new DocumentObject();
var config = require('../db/docDb');

document.init(config.veedb.databases.sensor.collections.rawdata, null);

var Csv = require('./csv');

var add = function(rawdata, callback) {
  document.addItem(rawdata, function(err, result) {
    if (err) {
      console.log('Error from documentDB: ' + JSON.stringify(err));
      return;
    }

    callback(null, result);
  });
};

var findLimit = function(config, callback) {
  var csv = new Csv();
  csv.init();
  var querySpec;
  if (config.amount === 0) {
    querySpec = {
      query: 'SELECT * r.rawData FROM root r where r.installationId=@id order by r.recordedAt',
      parameters: [{
        name: '@id',
        value: config.installationId
      }]
    };
  } else {
    querySpec = {
      query: 'SELECT top @top r.rawData FROM root r where r.installationId=@id order by r.recordedAt',
      parameters: [{
        name: '@id',
        value: config.installationId
      }, {
        name: '@top',
        value: config.amount
      }]
    };
  }


  document.findLimit(querySpec, 100, function(results, done) {
    var number = 0;
    if (!done) {
      for (var result in results) {
        console.log('rawdata: ' + number++);
        //console.log(results[result].rawData);
        csv.add(results[result].rawData);

      }

    } else {
      csv.export(function(csvStream) {
        callback(csvStream);
        csv.destroy();

      });
    }

  });
};

module.exports = {
  add: add,
  findLimit: findLimit
};