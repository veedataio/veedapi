'use strict';
var logTag = 'db.js: '
var config = require('../config/config');
var dbClient = require('documentdb').DocumentClient;

var findAll = function (callback) {
    console.log("Loading Database information")
    var client = new dbClient(config.dbUrl, {
        masterKey: config.dbMasterKey
    });
    var list = [];
    client.queryDatabases({
        query:'SELECT * FROM root r',
        parameters: []
    }).toArray(function(err, result) {
        if (err) {
            callback(err);
        }
        else {
            for (var i = 0; i < result.length; i++) {
                list.push(result[i].id);
            }
            for (var i = 0; i < result.length; i++) {
                list.push(result[i]._self);
            }
        }
        callback(list);
    })
};


module.exports = {
    findAll: findAll
};
