'use strict';

var log = require('../../config/log')(module);
var config = require('../../config/config');
var dbConfig = require('../../db/docDb');
var dbUrl = config.dbUrl;
var dbMasterKey = config.dbMasterKey;

var async = require('async');
var util = require('util');

var database = dbConfig.veedb.databases.statistics.name;
var collections = dbConfig.veedb.databases.statistics.collections;
var DocumentClient = require('documentdb').DocumentClient;

var client = new DocumentClient(dbUrl, {
  masterKey: dbMasterKey,
});

var _getStats = function(deviceId, startTime, endTime, type, callback) {
  var query = '' +
    'SELECT r.timestamp, r.activity, ' +
    'r.raw AS raw, r.heartrate ' +
    'FROM root r ' +
    'WHERE r.deviceid=@id ' +
    'AND (r.timestamp BETWEEN @startTime AND @endTime) ' +
    'ORDER BY r.timestamp DESC';

  var querySpec = {
    query: query,

    parameters: [
      {
        name: '@id',
        value: deviceId,
      },
      {
        name: '@startTime',
        value: startTime,
      },
      {
        name: '@endTime',
        value: endTime,
      },
    ],
  };

  log.debug('Query: ' + util.inspect(querySpec));

  var queryiterator = client.queryDocuments('dbs/' + database + '/colls/' + collections[type], querySpec);
  var allResults = [];
  execute(queryiterator, querySpec, function(results, isdone) {
    log.info(results);
    if (isdone) {
      callback({
        deviceid: deviceId,
        data: allResults,
      });
      return;
    }
    results.forEach(function(result) {
      allResults.push(result);
    });
  });
};

function execute(queryiterator, querySpec, callback) {
  queryiterator.executeNext(function(err, docs, headers) {
    log.charge('q2', {charge: headers['x-ms-request-charge'], querySpec: querySpec});

    if (err && err.code === 429 && headers['x-ms-retry-after-ms']) {

      var retryInMs = headers['x-ms-retry-after-ms'];
      log.debug('Request size too large, retry in %s', retryInMs);
      // Request size too large, retry after ms
      setTimeout(function() {
        execute(queryiterator, querySpec, callback);
      }, retryInMs);

    } else if (err && err.code !== 429) {
      log.error(err);

    } else {
      log.debug('headers: ' + headers);

      if (callback) {
        callback(docs, false);
      }

      if (queryiterator.hasMoreResults()) {
        execute(queryiterator, querySpec, callback);
      } else {
        callback(null, true);
      }
    }

  });

}

module.exports = {
  getStats: _getStats,
};
