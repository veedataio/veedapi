'use strict';
var logTag = 'collection.js: '
var dbClient = require('documentdb').DocumentClient;
var config = require('../config/config');
var dbUrl = config.dbUrl;
var dbMasterKey = config.dbMasterKey;

var findAll = function (db, callback) {
    var client = new dbClient(dbUrl, {
        masterKey: dbMasterKey
    });
    var list = [];
    client.queryCollections(db, {
        query: 'SELECT * FROM root r',
        parameters: []
    }).toArray(function (err, result) {
        if (err) {
            callback(err);
        }
        else {
            for (var i = 0; i < result.length; i++) {
                list.push(result[i].id);
            }
        }
        callback(list);
    })
};


module.exports = {
  findAll: findAll
};
