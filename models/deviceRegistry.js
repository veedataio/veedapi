/**
 * Created by Pablo on 8/26/2016.
 */
'use strict';
const async = require('async');
const log = require('../config/log')(module);
const iotErrors = require('azure-iot-common').errors;
const iotHub = require('azure-iothub');
const util = require('util');
const uuid = require('node-uuid');

const env = require('../config/config');
const registry = iotHub.Registry.fromConnectionString(env.connString);

module.exports = {
  create: function(callback) {
    log.info('create: Creating a new IoT Hub device.');

    var device = new iotHub.Device(null);
    device.deviceId = uuid.v4();
    log.debug('generated id = ' + device.deviceId);

    /*
     * A few things can happen when creating a new device, e.g. the device id already exists, invalid configuration or
     * simply a successful creation. To handle this situations a "do/while" loop is used using async.util.
     *
     * This is how the handling works:
     * 1. Successful creation: set isDone to true, then the test function ends the loop.
     * 2. Device already exists: set isDone to false, then the test function will generate a new id & the loop continues
     * 3. All other errors: set isDone to true as we might not be able to recover from them and end the loop.
     */
    var isDone = false;
    async.doUntil(
        function(asyncCallback) { // do: iterate until test returns true
          registry.create(device, function(err, deviceInfo, res) {

            if (err) {
              var errorMessage = _extractIoTErrorMessage(err);
              if (err instanceof iotErrors.DeviceAlreadyExistsError) {
                // iterate again by setting isDone to false and generate a new device id
                isDone = false;
                log.error('Will attempt to create a device with a different id. ', errorMessage);

              } else {
                // stop iterating by setting isDone to true
                isDone = true;
                log.error('Failed to create a device in IoT Hub:', errorMessage);
              }

              asyncCallback(new Error(errorMessage), deviceInfo);

            } else {
              isDone = true;
              asyncCallback(null, deviceInfo);
            }
          });
        },
        function() { // test: iterate until this function returns true
          log.debug('isDone =', isDone);

          // if not done, then generate a new device id
          if (!isDone) {
            device.deviceId = uuid.v4();
            log.debug('new generated id =', device.deviceId);
          }

          return isDone;
        },
        function(err, result) { // callback: call it after the test passes and repeated iterate function stops
          if (err) {
            log.log('create: failed to create a device in IoT hub: ', util.inspect(err));
            return callback(err);
          }

          log.log('create: Device created in IoT hub (id=%s)', util.inspect(result.deviceId));
          return callback(null, result.deviceId);
        }
    );
  },

  delete: function(deviceId, callback) {
    log.info('delete: Deleting IoT Hub device %s', deviceId);
    registry.delete(deviceId, function(err, nullArg, response) {
      if (err) {
        var errorMessage = _extractIoTErrorMessage(err);
        log.error('Failed to delete a device in IoT Hub:', errorMessage);
        callback(new Error(errorMessage));

      } else {
        log.info('IoT Hub device successfully deleted.');
        callback(null, response);
      }
    });
  },

  getConnectionString: function(deviceId, callback) {
    registry.get(deviceId, function(err, iotHubDevice, response) {
      if (err) {
        log.error('getConnectionString: Error retrieving the device from the IoT hub registry:', util.inspect(err));
        callback(new Error(_extractIoTErrorMessage(err)));

      } else {
        // RegEx for extracting 'HostName=' plus a URL from the Registry's connection string
        var regExp = /HostName=((?:[a-z|A-Z][a-z|A-Z.\d\-]+)\.(?:[a-z|A-Z][a-z|A-Z\-]+))(?![\w.])/g;
        var hostName = env.connString.match(regExp);

        // A connection string for a device follows the format "HostName=url;DeviceId=id;SharedAccessKey=abc"
        var connString = hostName + ';' +
            'DeviceId=' + deviceId + ';' + _constructAuthenticationString(iotHubDevice.authentication);

        callback(null, connString);
      }
    });
  }
};

function _constructAuthenticationString(authenticationMechanism) {
  var authString = '';

  if (authenticationMechanism.SymmetricKey.primaryKey) {
    authString = 'SharedAccessKey=' + authenticationMechanism.SymmetricKey.primaryKey;
  } else if (authenticationMechanism.x509Thumbprint.primaryThumbprint ||
      authenticationMechanism.x509Thumbprint.secondaryThumbprint) {
    authString = 'x509=true';
  }

  return authString;
}

function _extractIoTErrorMessage(err) {
  var errorMessage = JSON.parse(err.responseBody).Message;
  if (err instanceof iotErrors.DeviceNotFoundError) {
    errorMessage = 'Device was not found in IoT Hub. ' + errorMessage;

  } else if (err instanceof iotErrors.DeviceAlreadyExistsError) {
    errorMessage = 'Device already exists in IoT Hub. ' +
        errorMessage;

  } else if (err instanceof iotErrors.UnauthorizedError) {
    errorMessage = 'Check the connection string for IoT hub. ' + errorMessage;
  }

  return errorMessage;
}
