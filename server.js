'use strict';

var log = require('./config/log')(module);
var level = 'info';
if (process.env.NODE_ENV == 'test') {
  level = 'emerg'; // hide logs for tests
}

// This port assignment is required by iisnode
var port = process.env.PORT || 8000;

var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var swaggerize = require('swaggerize-express');
var swaggerUi = require('swaggerize-ui');
var path = require('path');
var winston = require('winston');
var expressWinston = require('express-winston');
var moment = require('moment');
var util = require('util');



var app = express();

var server = http.createServer(app);
app.use(bodyParser.json());

// Simple CORS middleware
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: false,
        colorize: true,
        level: level,
      }),
    ],
    msg: 'HTTP {{req.method}} {{req.url}}',
    expressFormat: true,
    colorize: true,
  }
));

var swaggerFile = path.join(__dirname, 'api') + '/swagger.yaml';
var handlers = path.join(__dirname, 'handlers');

app.use(swaggerize({
  api: path.resolve(swaggerFile),
  handlers: path.resolve(handlers),
  docspath: '/swagger',
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/docs', swaggerUi({
  docs: '/0.0.3/swagger',
}));

//TODO: check for necessary environment variables
server.listen(port, function() {
  log.info('Veedapi server has started');
});

module.exports = app;
