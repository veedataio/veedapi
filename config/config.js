'use strict';

var config = {
    dbUrl: process.env.DB_URL,
    dbMasterKey: process.env.DB_KEY,
    dbName: process.env.DB_NAME,
    dbTrainingRawCollection: process.env.DB_TRAINING_RAW_COLLECTION,
    connString: process.env.CONNECTION_STRING
};

module.exports = config;
