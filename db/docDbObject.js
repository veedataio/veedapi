'use strict';

//var DocumentDBClient = require('documentdb').DocumentClient;
var docdbUtils = require('./docDbUtils');
var util = require('util');
var config = require('./docDb');
var DbClient = require('documentdb').DocumentClient;

var envConfig = require('./../config/config');
var dbUrl = envConfig.dbUrl;
var dbMasterKey = envConfig.dbMasterKey;

var log = require('../config/log')(module);
/**
 *
 **/
function Document(documentDBClient, databaseId, collectionId) {
  this.client = documentDBClient;
  this.databaseId = databaseId;
  this.collectionId = collectionId;

  this.database = null;
  this.collection = null;
}

Document.prototype = {
  isInitialized: function() {
    if (this.collection && this.database) {
      return true;
    } else {
      return false;
    }
  },
  init: function(coll, callback) {
    log.debug('Initialize collection: ' + coll);
    var _this = this;

    // iterate through the configuration to determine host, database from given collection
    // TODO: optimize break of nested loops
    if (coll !== undefined) {
      var host;
      var db;
      var found = false;
      Object.keys(config).forEach(function(hostCfg) {
        if (found) {
          return;
        }

        host = hostCfg;
        Object.keys(config[host].databases).forEach(function(databaseCfg) {
          if (found) {
            return;
          }

          db = databaseCfg;
          Object.keys(config[host].databases[db].collections).forEach(
            function(collectionCfg) {
              if (collectionCfg === coll) {
                found = true;
              }
            });
        });
      });

      // now initialize the Document Object properly with determined configuration
      _this.client = new DbClient(dbUrl, {
          masterKey: dbMasterKey,
        }
      );
      _this.databaseId = config[host].databases[db].name;
      _this.collectionId = config[host].databases[db].collections[coll];
      if (callback) {
        callback();
      }

    }

    docdbUtils.getOrCreateDatabase(_this.client, _this.databaseId,
      function(err, dbObj) {
        if (err) {
          log.error('Error getOrCreateDatabase %d', err);
          callback(err);

        } else {

          _this.database = dbObj;
          docdbUtils.getOrCreateCollection(_this.client,
            _this.database._self, _this.collectionId,
            function(err, collObj) {
              if (err) {
                log.error('Collection not found.', err);
                callback(err);

              } else {
                _this.collection = collObj;

                if (callback) {
                  callback(null, true);
                }
              }
            });
        }
      });

  },

  find: function(querySpec, callback) {
    var self = this;


    var collectionToquery = self.collection._self;
    log.debug(self.collection);
    log.debug(collectionToquery);

    self.client.queryDocuments(collectionToquery, querySpec)
      .toArray(function(err, results) {
        if (err) {
          callback(err);

        } else {
          callback(null, results);
        }
      });
  },

  findLimit: function(querySpec, bucketsize, callback) {
    var self = this;

    var queryiterator = self.client.queryDocuments(self.collection._self,
      querySpec, {
        maxItemCount: bucketsize
      });

    execute(queryiterator, function(results, isdone) {
      callback(results, isdone);
    });

  },

  addItem: function(item, callback) {
    var self = this;

    var options = {
      //preTriggerInclude: "UUID"
    };
    self.client.createDocument(self.collection._self, item, options, function(err, doc) {
      if (err) {
        log.error('Error creating document.', err);
        callback(err);

      } else {
        callback(null, doc);
      }
    });
  },

  updateItem: function(item, callback) {
    var _this = this;
    //log.debug(item);

    _this.getItem(item.id, function(err, doc) {
      if (err) {
        callback(err);

      } else {
        if (doc.length === 0) {
          callback({
            message: 'Document with id ' + item.id + ' could not be found.',
          }, null);
        } else {

          // iterate through all items and copy the attributes.
          for (var property in item) {
            if (!(
              property === 'id' ||
              property === '_rid' ||
              property === '_self' ||
              property === '_etag' ||
              property === '_ts' ||
              property === '_attachments')) {

              // update attributes
              log.debug('replacing %j with %j', doc[property], item[property]);
              doc[property] = item[property];
            }

          }

          _this.client.replaceDocument(doc._self, doc, function(err, replaced) {
            if (err) {
              callback(err);

            } else {
              callback(null, replaced);
            }
          });
        }
      }
    });
  },

  getItem: function(itemId, callback) {

    var _this = this;

    var querySpec = {
      query: 'SELECT * FROM root r WHERE r.id=@id',
      parameters: [
        {
          name: '@id',
          value: itemId,
        },
      ],
    };

    _this.client.queryDocuments(_this.collection._self, querySpec).toArray(function(err, results) {
      if (err) {
        callback(err);
      } else {
        callback(null, results.length !== 0 ? results[0] : results);
      }
    });
  },

  deleteItem: function(itemId, callback) {
    var _this = this;

    _this.getItem(itemId, function(err, doc) {
      if (err) {
        log.error(err);
        callback(err, null);

      } else if (doc.length === 0) {
        callback({
          message: 'Document with id ' + itemId + ' could not be found.',
        }, null);

      } else {
        _this.client.deleteDocument(doc._self, function(err) {
          if (err) {
            callback(err, null);
          } else {
            callback(null, {
              message: 'deleted',
            });
          }
        });
      }
    });
  },

  createdStoredProcedure: function(procedure, callback) {
    var _this = this;

    _this.client.createStoredProcedure(_this.collection._self, procedure)
      .then(function(response) {
        console.log('Successfully created stored procedure');
        callback(response.resource);
        console.log('Successfully created stored procedure');
      }, function(error) {
        console.log('Error', error);
      });
  },
};

function execute(queryiterator, callback) {
  queryiterator.executeNext(function(err, docs, headers) {
    //console.log(docs.length);
    console.log(headers);
    //console.log(queryiterator.hasMoreResults());
    if (callback) {
      callback(docs, false);
    }

    if (queryiterator.hasMoreResults()) {
      execute(queryiterator, callback);
    } else {
      callback(null, true);
    }

  });

}

module.exports = Document;