'use strict';

var config = {
  veedb: {
    databases: {
      sensor: {
        name: 'sensor_data',
        collections: {
          rawdata: 'rawdata',
        },
      },
      service: {
        name: 'service',
        collections: {
          users: 'users',
          activity: 'activity',
          feed: 'feed',
          devices: 'devices',
        },
      },
      statistics: {
        name: 'statistics',
        collections: {
          min: 'devices-minutes-five',
          day: 'devices-day',
          hour: 'devices-hour',
          week: 'devices-week',
          month: 'devices-month',
          year: 'devices-year',
        },
      },
    },
  },
};

module.exports = config;
