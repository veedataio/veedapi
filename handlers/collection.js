'use strict';

var collectionModel = require('../models/collection');



module.exports = {
  /**
   * list devices from server
   * produces:
   *   array of device
   */
    get: function (req, res) {
        var db = req.query.db;
        collectionModel.findAll(db, function (results) {
            res.send(results);
      });

  }
};
