'use strict';

var feed = require('../models/feed');
var log = require('../config/log')(module);

module.exports = {
  post: function(req, res) {
    var newFeed = req.body;
    log.info(newFeed);

    feed.add(newFeed, function(result) {
      log.info('New feed added %d', result);
      res.json(result);
    });
  },

  get: function(req, res) {
    var tags = req.query.tags || [];
    if (tags.length === 0) {
      feed.findAll(function(feed) {
        res.json(feed);
      });
    } else {
      feed.findByTags(tags, function(feed) {
        res.json(feed);
      });
    }
  },

};