'use strict';

var dbModel = require('../models/db');



module.exports = {
  /**
   * list devices from server
   * produces:
   *   array of device
   */

  get: function(req, res) {
      dbModel.findAll(function (results) {
          res.send(results);
    });

  }
};
