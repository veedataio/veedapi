'use strict';
var log = require('../../../config/log')(log);
var StatsModel = require('../../../models/device/stats');

var getStats = function(req, res) {
  var deviceId = req.params.deviceId;
  var startTime = req.query.startTime;
  var endTime = req.query.endTime;
  var type = req.query.timeInterval;

  StatsModel.getStats(deviceId, startTime, endTime, type, function(result, error) {
    if (error) {
      log.error(error);
      var errorMessage = {
        message: error.message,
      };
      res.status(500).send(errorMessage);
    } else {
      res.send(result);
    }

  });

};

module.exports = {
  get: getStats,
};

