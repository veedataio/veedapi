'use strict';

var feed = require('../../models/feed');
var log = require('../../config/log')(module);

module.exports = {
  get: function(req, res) {
    var id = req.params.id;

    feed.find(id, function(result) {
      //TODO: 404
      log.info(result);
      res.json(result);
    });

  },

  delete: function(req, res) {
    log.info('Delete feed', req.params);
    var id = req.params.id;

    feed.delete(id, function(error, result) {
      if (error) {
        //TODO: Should be 404 && 500
        res.status(500).send(error);
      } else {

        res.json(result);
      }

    });
  },

  put: function(req, res) {
    log.info('Update feed', req.body);
    var feedDoc = req.body;

    feed.update(feedDoc, function(error, result) {
      if (error) {
        //TODO: Should be 404 && 500
        res.status(500).send(error.message);
      } else {
        res.json(result);
      }
    });

  },

};