'use strict';

var rrInterval = require('../../models/band/rrinterval');

module.exports = {
    post: function (req, res) {
        if (!req.body) return res.sendStatus(400);
        var eventData = req.body;

        console.log('eventData: ' + eventData);
        console.log(eventData);

        rrInterval.add(eventData, function (err, result) {
            if (err) {
                res.status(500); // error
                res.json({
                    error: err
                });
            }
            res.status(201);
            res.json(result);
        });

    }
};
