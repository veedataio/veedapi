/**
 * Created by Pablo on 10/12/2016.
 */
'use strict';
var log = require('../../config/log')(module);

var DeviceModel = require('../../models/device');

var _put = function(req, res) {
  var deviceId = req.params.id;
  var meta = req.body;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error('delete: ' + err);
      var errorMessage = {
        message: err.message
      };
      res.status(404).send(errorMessage);

    } else {
      // update
      if (meta.alias) {
        log.info('New Alias: %s', meta.alias);
        log.info('Device %s', device);
        device.data.alias = meta.alias;
        DeviceModel.update(device.data, function(err, result) {
          res.send(result);
        });
      } else {
        res.status(500).send({
          message: 'Only attribute alias can be udpated.',
        });
      }
    }
  });

};

var _delete = function deleteDevice(req, res) {
  var deviceId = req.params.id;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error('delete: ' + err);
      var message = {
        message: err.message
      };
      res.status(404).send(message);

    } else {
      DeviceModel.delete(deviceId, function(err) {
        if (err) {
          log.error(err);
          var message = {
            message: err.message
          };
          log.info('Sending back status 500,', message);
          res.status(500).send(message);

        } else {
          log.info('Sending back status 200.');
          res.status(200).send();
        }
      });
    }
  });
};

module.exports = {
  put: _put,
  delete: _delete
};
