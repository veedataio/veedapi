/**
 * Created by Pablo on 10/6/2016.
 */
'use strict';
var codename = require('codename')();
var log = require('../../config/log')(module);

var DeviceModel = require('../../models/device');

module.exports = {
  post: function addDevice(req, res) {
    var alias = req.query.alias;
    var userId = req.query.userId;

    DeviceModel.create(alias, userId, function(err, deviceSummary) {
      if (err) {
        log.error(err);
        var message = {
          message: err.message
        };
        log.info('Sending back status 500,', message);
        res.status(500).send(message);

      } else {
        log.info('Sending back status 201,', deviceSummary);
        res.status(201).send(deviceSummary);
      }
    });
  }
};
