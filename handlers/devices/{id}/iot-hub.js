/**
 * Created by Pablo on 9/1/2016.
 */
'use strict';

var log = require('../../../config/log')(module);

var DeviceModel = require('../../../models/device');

var getIotHubInfo = function(req, res) {
  var deviceId = req.params.id;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error('findById: ', err);

      var errorMessage = {
        message: err.message
      };
      res.status(404).send(errorMessage);

    } else {
      device.getIotHubInfo(function(err, iotHubInfo) {
        if (err) {
          log.error('getIotHubInfo: Unable to retrieve IoT Hub information for device %s. %j', deviceId, err.message);
          var errorMessage = {
            message: err.message
          };
          res.status(400).send(errorMessage);
        } else {
          log.info('getIotHubInfo: Sending back: %s', iotHubInfo);
          res.send(iotHubInfo);
        }
      });
    }
  });
};

module.exports = {
  get: getIotHubInfo
};
