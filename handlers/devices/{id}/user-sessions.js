/**
 * Created by Pablo on 8/25/2016.
 */
'use strict';

var logTag = 'devices/user-sessions.js: ';

var DeviceModel = require('../../../models/device');

var createUserSession = function (req, res) {
    res.status(400).send('{ message: "Operation not supported." }');
};

module.exports = {
    put: createUserSession
};
