/**
 * Created by Pablo on 8/31/2016.
 */
'use strict';

var log = require('../../../../config/log')(module);
var moment = require('moment');

var DeviceModel = require('../../../../models/device');

var endUserSession = function(req, res) {
  var deviceId = req.params.id;
  var userId = req.query.userId;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error(err);
      var message = {
        message: err.message
      };
      res.status(404).send(JSON.stringify(message));

    } else {
      var endTime = moment.utc().toJSON();
      device.endUserSession(userId, endTime, function(err, session) {
        if (err) {
          var message = 'Unable to end user session. ' + err.message;
          log.error(message);
          res.status(400).send('{ message: ' + message + '}');

        } else {
          log.info('Success, ended user session id = ' + session.sessionId);
          res.status(200).send(session);
        }
      });
    }
  });
};

module.exports = {
  post: endUserSession
};
