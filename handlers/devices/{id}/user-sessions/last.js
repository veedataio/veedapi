/**
 * Created by Pablo on 9/12/2016.
 */
'use strict';

var log = require('../../../../config/log')(module);

var DeviceModel = require('../../../../models/device');

var getLastUserSession = function(req, res) {
  var deviceId = req.params.id;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error(err);
      var message = {
        message: err.message
      };
      res.status(404).send(JSON.stringify(message));

    } else {
      var lastSession = device.getLastUserSession();
      if (lastSession.length <= 0) {
        log.info('No user session. Sending back no content.');
        res.status(204).send();

      } else {
        log.info('Sending back [' + lastSession + ']');
        res.send(lastSession);
      }
    }
  });
};

module.exports = {
  get: getLastUserSession
};
