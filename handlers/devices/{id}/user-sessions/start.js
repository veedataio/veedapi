/**
 * Created by Pablo on 8/31/2016.
 */
'use strict';

var log = require('../../../../config/log')(module);
var moment = require('moment');

var DeviceModel = require('../../../../models/device');

var startUserSession = function(req, res) {
  var deviceId = req.params.id;
  var userId = req.query.userId;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error(err);
      var errorMessage = {
        message: err.message
      };
      res.status(404).send(errorMessage);

    } else {
      var startTime = moment.utc().toJSON();
      device.createUserSession(userId, startTime, function(err, session, isNewSession) {
        if (err) {
          var errorMessage = {
            message: err.message,
          }
          log.error(errorMessage);
          res.status(400).send(errorMessage);

        } else {
          log.info('Success, started user session id = ' + session.sessionId);
          if (isNewSession) {
            res.status(201);
          } else {
            res.status(200);
          }

          res.send(session);
        }
      });
    }
  });
};

module.exports = {
  post: startUserSession
};
