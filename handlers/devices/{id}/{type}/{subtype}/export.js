'use strict';

var log = require('../../../../../config/log')(module);
log.info('export');
var DataModel = require('../../../../../models/data');
var moment = require('moment');

var _get = function(req, res) {
  var deviceId = req.params.id;
  var type = req.params.type;
  var subtype = req.params.subtype;

  var startTime = req.query.startTime;
  var endTime = req.query.endTime;

  var fileType = req.query.fileType;

  startTime = moment.utc(startTime).toJSON();
  endTime = moment.utc(endTime).toJSON();

  log.debug(deviceId, type, subtype);

  DataModel.getDataAsFile(deviceId, type, subtype, startTime, endTime, fileType, function(error, results) {
    log.info('Results from db: ', results);
    res.send(results);
  });

};

module.exports = {
  get: _get,
};
