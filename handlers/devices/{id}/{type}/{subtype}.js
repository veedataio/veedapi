'use strict';

var log = require('../../../../config/log')(module);

var DataModel = require('../../../../models/data');
var moment = require('moment');

var _get = function(req, res) {
  var deviceId = req.params.id;
  var type = req.params.type;
  var subtype = req.params.subtype;

  var startTime = req.query.startTime;
  var endTime = req.query.endTime;

  var interval = req.query.interval;

  startTime = moment.utc(startTime).toJSON();
  endTime = moment.utc(endTime).toJSON();

  log.debug(deviceId, type, subtype);
  if (interval) {
    DataModel.getDataAggregated(deviceId, type, subtype, startTime, endTime, interval, function(error,results) {
      log.info('Results from db: ', results);
      res.send(results);
    });
  } else {
    DataModel.getData(deviceId, type, subtype, startTime, endTime, function(results) {
      log.info('Results from db: ', results);
      res.send(results);
    });
  }


};

module.exports = {
  get: _get,
};
