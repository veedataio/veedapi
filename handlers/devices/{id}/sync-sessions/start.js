/**
 * Created by Pablo on 9/19/2016.
 */
'use strict';
var log = require('../../../../config/log')(module);

var DeviceModel = require('../../../../models/device');
var moment = require('moment');

var startSyncSession = function(req, res) {
  var deviceId = req.params.id;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error(err);
      var message = {
        message: err.message
      };
      res.status(404).send(message);

    } else {
      var startTime = moment.utc().toJSON();

      device.createSyncSession(startTime, function(err, sessionId) {
        if (err) {
          var errorMessage = {
            message: err.message,
          };
          log.error(errorMessage);
          res.status(409).send(errorMessage);

        } else {
          log.info('Success, started user session id = ' + sessionId);
          res.status(201).send(sessionId);
        }
      });
    }
  });

};

module.exports = {
  post: startSyncSession
};
