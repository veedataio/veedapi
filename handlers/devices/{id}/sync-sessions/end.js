/**
 * Created by Pablo on 10/3/2016.
 */
'use strict';
var log = require('../../../../config/log')(module);

var DeviceModel = require('../../../../models/device');
var moment = require('moment');

var endSyncSession = function(req, res) {
  var deviceId = req.params.id;

  DeviceModel.findById(deviceId, function(err, device) {
    if (err) {
      log.error(err);
      var errorMessage = {
        message: err.message
      };
      res.status(404).send(errorMessage);

    } else {
      var stopTime = moment.utc().toJSON();

      device.endSyncSession(stopTime, function(err, sessionId) {
        if (err) {
          var errorMessage = {
            message: err.message
          };
          log.error(errorMessage);
          res.status(409).send(errorMessage);

        } else {
          log.info('Success, ended user session id = ' + sessionId);
          res.status(200).send(sessionId);
        }
      });
    }
  });
};

module.exports = {
  post: endSyncSession
};
