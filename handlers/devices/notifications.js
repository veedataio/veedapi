'use strict';
var log = require('../../config/log')(module);

var Notifications = require('../../models/devices/notifications');
var notes = new Notifications();

var sendNotification = function(req, res) {
  var deviceId = req.query.deviceId;

  var message = {
    title: req.query.title || '',
    body: req.query.body,
    badge: req.query.badge || 0,
    sound: req.query.sound,
    payload: req.query.payload || {},
  };

  log.info(message);


  notes.push(deviceId, message, function(err, success) {
    if (err) {
      log.error(err);
    } else {
      res.send(success);
    }
  });


};

module.exports = {
  post: sendNotification
};
