'use strict';
var log = require('../config/log')(module);

var DeviceModel = require('../models/device');

module.exports = {
  get: function(req, res) {
    log.info('Entering devices.get');

    DeviceModel.getAllDevices(function(err, results) {
      if (err) {
        var errorMessage = {
          message: err.message,
        };
        log.error('Sending back a 400 due to error %s. Cause: %s', err.message, err.cause);
        res.status(400).send(errorMessage);

      } else {
        log.info('Sending back the following results: ' + JSON.stringify(results));
        res.send(results);
      }
    });
  }
};
