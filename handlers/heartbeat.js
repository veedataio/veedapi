'use strict';

module.exports = {
  get: function (req, res) {
    console.log('get heartbeat');
    var heartbeat = {
      version: '0.0.1',
      alive: true
    };
    res.json(heartbeat);
  }
};