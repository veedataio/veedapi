'use strict';
require('dotenv').config({
  path: __dirname + '/../.env',
});

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let moment = require('moment');

let log = require('../config/log')(module);

const version = '/0.0.2';

chai.use(chaiHttp);

let devicesCreated = [];
let agent = chai.request(server);

describe('Devices', function() {
  this.timeout(60000);

  before(function(done) {
    this.timeout(8000);
    log.info('Wait a moment for server to load.');
    setTimeout(function() {
      done();
    }, 5000);
  });

  after(function(done) {
    log.info('final clean up');
    done();
  });

  /*
   * Test the /GET route
   */
  describe('POST /devices', function() {
    this.timeout(5000);

    it('201 - Create a new device with no attributes set.', function(done) {
      agent.post(version + '/devices/new')
        .end(function(err, res) {
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.id.length.should.equal(36);
          res.body.should.have.property('alias');

          devicesCreated.push(res.body.id);
          done();
        });
    });

    let newDevice = {
      alias: 'DeviceAlias',
      userId: 'Chai',
    };

    it('201 - Create a new device with attributes set.', function(done) {
      agent.post(version + '/devices/new')
        .query(newDevice)
        .end(function(err, res) {
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          res.body.id.length.should.equal(36);
          res.body.should.have.property('alias');
          res.body.alias.should.equal(newDevice.alias);
          res.body.should.have.property('currentUser');
          res.body.currentUser.should.equal(newDevice.userId);

          devicesCreated.push(res.body.id);
          log.debug('Devices created', devicesCreated);
          done();
        });
    });

  });

  describe('GET /devices', function() {
    this.timeout(5000);
    it('200 - Retrieving all registered devices.', function(done) {
      agent.get(version + '/devices')
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.at.least(1);
          res.body[0].should.be.a('object');
          res.body[0].should.have.property('id');
          res.body[0].should.have.property('alias');

          done();
        });
    });

  });

  describe('GET /devices/{id}', function() {
    this.timeout(15000);

    it('200 - Retrieving the connection string from iot-hub', function(done) {
      let deviceId = devicesCreated[1]; // let's use the last created test device.

      agent.get(version + '/devices/' + deviceId + '/iot-hub')
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('connString');

          done();
        });
    });

    it('404 - Retrieving the connection string of a non-registered device.', function(done) {
      agent.get(version + '/devices/dev-test-chai/iot-hub')
        .end(function(err, res) {
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.message.should.equal('Device \"dev-test-chai\" not found.');
          done();
        });
    });

  });

  describe('GET /device/{id}/sync-sessions/', function() {
    this.timeout(15000);

    // TODO: once endpoints are available, implement
    it('Create a new device to run tests.', function(done) {
      done();
    });

    describe('GET /device/{id}/sync-sessions/start', function() {

      it('404 - Start a new sync-session with a non-existing device.', function(done) {
        agent.post(version + '/devices/dev-test-chai/sync-sessions/start')
          .end(function(err, res) {
            res.should.have.status(404);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.message.should.equal('Device "dev-test-chai" not found.');
            done();
          });

      });

      it('201 - Start a new sync-session.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/sync-sessions/start')
          .end(function(err, res) {
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');
            res.body.endTime.length.should.equal(0);
            done();
          });

      });

      it('409 - Start a new sync-session after one has been previously started.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/sync-sessions/start')
          .end(function(err, res) {
            res.should.have.status(409);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            done();
          });

      });
    });

    describe('GET /device/{id}/sync-sessions/stop', function() {

      it('200 - Stop the sync-session.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/sync-sessions/end')
          .end(function(err, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');
            moment(res.body.endTime).isValid().should.equal(true);
            done();
          });

      });

      it('409 - Stop the sync-session that has been previously stopped.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/sync-sessions/end')
          .end(function(err, res) {
            res.should.have.status(409);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.message.should.equal('Can\'t end a sync session, \"' + deviceId + '\" has not an opened session.');
            done();
          });

      });

      it('404 - Stop the sync-session of a non-registered device', function(done) {
        agent.post(version + '/devices/dev-test-chai/sync-sessions/end')
          .end(function(err, res) {
            res.should.have.status(404);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.message.should.equal('Device "dev-test-chai" not found.');
            done();
          });

      });
    });
  });

  describe('GET /device/{id}/user-sessions/', function() {
    this.timeout(15000);

    // TODO: once endpoints are available, implement
    it('Create a new device to run tests.', function(done) {
      done();
    });

    describe('POST /device/{id}/user-essions/start', function() {

      it('404 - Start a new user-session with a non-existing device.', function(done) {
        agent.post(version + '/devices/dev-test-chai/user-sessions/start')
          .query({userId: 'mocha-test-user'})
          .end(function(err, res) {
            res.should.have.status(404);
            res.body.should.be.a('object');
            res.body.should.have.property('message');
            res.body.message.should.equal('Device "dev-test-chai" not found.');
            done();
          });

      });

      //TODO: Not sure how to test this.
      it('400 - ', function(done) {
        done();
      });

      it('201 - Start a new user-session.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/user-sessions/start')
          .query({userId: 'mocha-test-user'})
          .end(function(err, res) {
            res.should.have.status(201);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('userId');
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');
            res.body.endTime.length.should.equal(0);

            done();
          });

      });

      it('200 - Start a new user-session again.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/user-sessions/start')
          .query({userId: 'mocha-test-user'})
          .end(function(err, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('userId');
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');
            res.body.endTime.length.should.equal(0);

            done();
          });

      });
    });

    describe('GET /device/{id}/user-essions/last', function() {

      it('200 - Retrieve the last user-session.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.get(version + '/devices/' + deviceId + '/user-sessions/last')
          .end(function(err, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('userId');
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');

            done();
          });

      });

    });

    describe('GET /device/{id}/user-essions/end', function() {
      it('200 - End the user-session.', function(done) {
        let deviceId = devicesCreated[1]; // let's use the last created test device.
        agent.post(version + '/devices/' + deviceId + '/user-sessions/end')
          .query({userId: 'mocha-test-user'})
          .end(function(err, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('sessionId');
            res.body.sessionId.length.should.equal(36);
            res.body.should.have.property('userId');
            res.body.should.have.property('startTime');
            moment(res.body.startTime).isValid().should.equal(true);
            res.body.should.have.property('endTime');
            moment(res.body.endTime).isValid().should.equal(true);
            done();
          });

      });
    });

  });

  describe('DELETE /devices/{id}', function() {
    this.timeout(15000);
    let deviceId = devicesCreated[1]; // let's use the last created test device.

    it('200 - Delete a device', function(done) {
      let deviceId = devicesCreated[1]; // let's use the last created test device.
      agent.delete(version + '/devices/' + deviceId)
        .end(function(err, res) {
          res.should.have.status(200);

          done();
        });
    });

    it('404 - Delete a device that does not exist.', function(done) {
      agent.delete(version + '/devices/dev-test-chai')
        .end(function(err, res) {
          res.should.have.status(404);
          res.body.should.be.a('object');
          res.body.should.have.property('message');
          res.body.message.should.equal('Device \"dev-test-chai\" not found.');
          done();
        });
    });

  });

  describe('GET /devices/{id}/{type}/{subtype}', function() {
    this.timeout(15000);

    it('200 - Retrieve sensor / heart-rate data', function(done) {
      let deviceId = devicesCreated[0]; // let's use the last created test device.
      agent.delete(version + '/devices/' + deviceId + '/sensor/heart-rate')
        .end(function(err, res) {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.at.least(1);
          res.body[0].should.be.a('object');
          res.body[0].should.have.property('deviceId');
          res.body[0].should.have.property('dataType');
          res.body[0].should.have.property('dataSubtype');
          res.body[0].should.have.property('data');

          done();
        });
    });

  });

});
