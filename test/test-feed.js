'use strict';
require('dotenv').config({
  path: __dirname + '/../.env',
});

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

let log = require('../config/log')(module);

const version = '/0.0.2';

chai.use(chaiHttp);


describe('Feed', function(done) {
  let agent = chai.request(server);
  let idsCreated = [];

  before(function(done) {
    this.timeout(5000);
    log.info('Wait a moment for server to load.');
    setTimeout(function() {
      done();
    }, 3000);
  });

  /*
   * Test the /GET route
   */
  describe('GET /feed', function() {
    this.timeout(60000);

    it('200 - should retrieving all feeds', (done) => {
      agent.get(version + '/feed')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.at.least(1);
          res.body[0].should.be.a('object');
          res.body[0].should.have.property('id');
          res.body[0].should.have.property('title');
          res.body[0].should.have.property('content');
          res.body[0].should.have.property('likes');
          res.body[0].should.have.property('tags');
          res.body[0].tags.should.be.a('array');
          res.body[0].tags.length.should.be.at.least(1);
          res.body[0].should.have.property('id');
          res.body[0].should.have.property('_rid');
          res.body[0].should.have.property('_self');
          res.body[0].should.have.property('_etag');
          res.body[0].should.have.property('_attachments');
          res.body[0].should.have.property('publishDate');
          res.body[0].should.have.property('likes');
          res.body[0].should.have.property('_ts');

          done();
        });
    });


    it('200 - should retrieve one feed item.', function(done) {
      agent.get(version + '/feed')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.at.least(1);
          res.body[0].should.be.a('object');
          res.body[0].should.have.property('id');
          let id = res.body[0].id;
          agent.get(version + '/feed' + id)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('id');
              res.body.should.have.property('title');
              res.body.should.have.property('content');
              res.body.should.have.property('likes');
              res.body.sould.have.property('tags');
              res.body.tags.should.be.a('array');
              res.body.tags.length.should.be.at.least(1);
              res.body.should.have.property('id');
              res.body.should.have.property('_rid');
              res.body.should.have.property('_self');
              res.body.should.have.property('_etag');
              res.body.should.have.property('_attachments');
              res.body.should.have.property('publishDate');
              res.body.should.have.property('likes');
              res.body.should.have.property('_ts');
            });
          done();
        });
    });

    //TODO: implement
    it('should retrieve feed items based on tags', function(done) {
      // first create items with tags

      // then retrieve them based on the tags
      done();
    });

  });

  describe('POST /feed', () => {
    this.timeout(15000);
    let postedItemId;

    let feed = {
      title: 'This is a feed title for testing.',
      content: 'This is some awesome content which is not overly lenghtly.',
      likes: 1,
      image: 'someStringPathToAnUrl.jpg',
      tags: [
        'tag1',
        'tag2'
      ],
      publishDate: 'date'
    };

    it('200 - should post a feed item.', function(done) {

      agent.post(version + '/feed')
        .send(feed)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('id');
          let id = res.body.id;
          // keep track of created ids
          idsCreated.push(id);
          postedItemId = id;

          agent.get(version + '/feed/' + id)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.title.should.equal(feed.title);
              res.body.content.should.equal(feed.content);
              res.body.likes.should.equal(feed.likes);
              res.body.image.should.equal(feed.image);
              res.body.tags.length.should.equal(feed.tags.length);
              res.body.tags[0].should.equal(feed.tags[0]);
              res.body.tags[1].should.equal(feed.tags[1]);
              res.body.publishDate.should.equal(feed.publishDate);

              done();
            });

        });
    });
    it('200 - should be retrieved and be the same', function(done) {

      if (postedItemId) {
        agent.get(version + '/feed/' + postedItemId)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.title.should.equal(feed.title);
            res.body.content.should.equal(feed.content);
            res.body.likes.should.equal(feed.likes);
            res.body.image.should.equal(feed.image);
            res.body.tags.length.should.equal(feed.tags.length);
            res.body.tags[0].should.equal(feed.tags[0]);
            res.body.tags[1].should.equal(feed.tags[1]);
            res.body.publishDate.should.equal(feed.publishDate);

            done();
          });
      } else {
        done('Feed item could not be posted, therefore it can not be retrieved.');
      }

    });

  });

  describe('DELETE /feed', () => {
    let feedItemIdToDelete;

    it('200 - should first create a new feed item.', function(done) {
      let feed = {
        title: 'This is a feed title for testing.',
        content: 'This is some awesome content which is not overly lenghtly.',
        likes: 1,
        image: 'someStringPathToAnUrl.jpg',
        tags: [
          'tag1',
          'tag2'
        ],
        publishDate: 'date'
      };

      agent.post(version + '/feed')
        .send(feed)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('id');
          feedItemIdToDelete = res.body.id;

          done();
        });
    });

    it('200 - should now delete the freshly created item.', function(done) {
      agent.delete(version + '/feed/' + feedItemIdToDelete)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('message');
          res.body.message.should.equal('deleted');

          done();
        });
    });

    it('500 - should now delete the freshly created item again and fail.', function(done) {
      agent.delete(version + '/feed/' + feedItemIdToDelete)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.have.property('message');
          res.body.message.should.equal('Document with id ' + feedItemIdToDelete + ' could not be found.');

          done();
        });
    });

    it('200 - should now not be able to get the feed back from the API.', function(done) {
      agent.get(version + '/feed/' + feedItemIdToDelete)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('message');
          res.body.message.should.equal('Document with id ' + feedItemIdToDelete + ' could not be found.');

          done();
        });
    });


  });

});
