{
    "_id": {
        "$oid": "56f5dddae4b080b1e4614a5f"
    },
    "user_id": "1001",
    "username": "Alex",
    "email": "alex@veedata.io",
    "password": "string",
    "interests": [
        "string"
    ],
    "devices": [{
        "device": "fitbit",
        "authentication": {
            "scope": "profile",
            "user_id": "4F6WXT",
            "token_type": "Bearer",
            "expires_in": "249019",
            "access_token": "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0NTk1MzU4NzEsInNjb3BlcyI6Indwcm8iLCJzdWIiOiI0RjZXWFQiLCJhdWQiOiIyMjdQM0siLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJpYXQiOjE0NTkyODY4NTJ9.3iDsxwaBE6N9JEMK2IMz8HVNbb-BjKeYPiSwLBVhfac"
        }
    }],
    "activities": {
        "active": {
            "current": "string",
            "goal": "string"
        },
        "steps": [
            0
        ]
    },
    "dailyscore": {
        "score": 0,
        "rating": 0
    }
}
