# veedapi

# Configuration
## Environment Variables

This is a list of all process environments used by Veedata.

| Environment Variable  									| Value  	|
|---																			|---			|
| process.env.DB_URL  										| # |
| process.env.DB_MASTERKEY  							| # |
| process.env.DB_NAME  										| # |
| process.env.DB_TRAINING_RAW_COLLECTION 	| #	|
| process.env.CONNECTION_STRING 					| # |
| process.env.PORT 												| # |



